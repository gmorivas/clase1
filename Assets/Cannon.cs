﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    // get a reference to a game object in  the editor
    public GameObject original;
    public Transform shootingPoint;

    private IEnumerator enumerador;
    private Coroutine corrutina;

    // Start is called before the first frame update
    void Start()
    {
        enumerador = Disparo();
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        transform.Translate(h * 5 * Time.deltaTime, 0, 0);

        if (Input.GetKeyDown(KeyCode.Space)) {
            // how to create new instances of a game object
            // there is no "new" for a game object
            // when we instantiate we clone

            // para lograr el efecto de "concurrencia" tiene que ejecutarse
            // startcoroutine
            corrutina = StartCoroutine(enumerador);
            
        }

        if (Input.GetKeyUp(KeyCode.Space)) {

            StopCoroutine(corrutina);
            // StopCoroutine(enumerador);
            //StopAllCoroutines();
        }
    }

    // coroutine
    // - pseudo threads
    // - runs consecutively

    public IEnumerator Disparo() {

        while (true) {

            Instantiate(
                original,
                shootingPoint.position,
                original.transform.rotation);
            yield return new WaitForSeconds(0.2f);
        }
    }
}

