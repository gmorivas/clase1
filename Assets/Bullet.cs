﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        // how to retrieve a reference
        // to an object (component) that is part of the same
        // game object
        // this can be null if you have no component of the type 
        // you're searching
        rb = GetComponent<Rigidbody>();

        // add force uses world space - not local!
        // 3 vectors that are useful to refer to directions
        // regarding the object but in world space
        // -up
        // -right
        // -forward
        // all of them unit vectors
        rb.AddForce(transform.up * 10, ForceMode.Impulse);

        // Destroy - remove a component or gameobject
        Destroy(gameObject, 5);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
