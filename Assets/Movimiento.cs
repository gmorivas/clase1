﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movimiento : MonoBehaviour
{
    // attributes ! 
    // any public attribute that's serializable can be exposed
    // to the editor
    public float speed = 5;
    public string example1;
    public bool example2;
    public Text textito;
    

    // life cycle
    // - set of methods that are invoked in a certain time during execution 

    // we do not dictate the logic flow, we adapt. 
    // unlike other applications there is no main!!!

    // avoid using constructors! anything that extends monobehaviour
    // must have empty default constructor
    
    // awake and start are useful for setup
    // the first method that runs on creation
    void Awake() {
        print("I'M AWAKE.");
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("STARTING!");
        textito.text = "HOLA AMIGUITOS!";
    }

    // Update is called once per frame
    // frame?! - amount of time between.... 
    
    // LOOP - how the engine (or any graphical app works):
    // - logic runs
    // - graphics are updated

    // measured in FPS - frames per second
    // desirable fps - as many as you can! 
    // good - 60 fps
    // minimum to consider an application realtime - 30 fps
    void Update()
    {
        // print is a non-blocking operation
        //print("UPDATE");

        // 2 things to run here:
        // - input: we want the game to be as responsive as possible
        // - motion: we want motion to be as smooth as possible
        // NOTHING ELSE.

        // how to detect input - 
        // 1. directly polling to the device
        // 2. through the use of axes(axis - singular, axes - plural)

        // 1. how to poll a device
        // (ask keyboard or mouse if something happened)

        if (Input.GetKeyDown(KeyCode.A)) {
            // if key was up last frame
            // and its down in this frame
            //print("KEY DOWN");
        }

        if (Input.GetKey(KeyCode.A))
        {
            // if key was down last frame
            // and it is still down
            //print("KEY");
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            // if key was down last frame
            // and its up this frame
            //print("KEY UP");
        }

        if (Input.GetMouseButtonDown(0)) {

            //print("MOUSE DOWN " + Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {

            //print("MOUSE");
        }

        if (Input.GetMouseButtonUp(0))
        {

            //print("MOUSE UP");
        }

        // the axes have a float value with a range
        //  [-1, 1]

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        // Transform and transform
        // Transform - a class
        // transform - an object that we get through inheritance

        // PROBLEM - it will move h units per frame
        // Time.deltaTime - how much time in seconds has passed
        // from the last frame to the current one 
        transform.Translate(
            h * speed * Time.deltaTime, 
            v * speed * Time.deltaTime,
            0,
            Space.World);
    }

    // happens on the main loop
    // happens after ALL the Updates
    void LateUpdate() {
        //print("LATE UPDATE");

        // veeeery specific use case
        // if you need to setup something before next update
    }

    // update that happens in a fixed interval
    // fixed - establecido, fijo, puesto, etc.
    void FixedUpdate() {

        //print("FIXED UPDATE");
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("ENTER ");
    }

    private void OnCollisionStay(Collision collision)
    {
        print("STAY");
    }

    private void OnCollisionExit(Collision collision)
    {
        print("EXIT");
    }

    private void OnTriggerEnter(Collider other)
    {
        print("TRIGGER ENTER " + other.gameObject.layer);
        print("TRIGGER ENTER " + other.transform.name);
    }

    private void OnTriggerStay(Collider other)
    {
        print("TRIGGER STAY");
    }

    private void OnTriggerExit(Collider other)
    {
        print("TRIGGER EXIT");
    }
}
